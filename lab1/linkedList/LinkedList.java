//package lab1.linkedList;

import java.lang.IllegalArgumentException;

public class LinkedList {
 
	private int counter;
	private Node head;
 
	public LinkedList() {}
 
	// appends the specified element to the end of this list.
	public void add(Object data) {
 		if(head == null) { 
 			head = new Node(data);
 			counter++;
 			return;
 		}
 		
 		Node copy = head;
 		while(copy.getNext() != null) {
 			copy = copy.getNext();
 		}
 		copy.setNext(new Node(data));
 		
 		counter++;			
	}
  
	// inserts the specified element at the specified position in this list
	public void add(Object data, int index) {
		if(index < 0 || (index >= counter && index != 0)) 
			throw  new IllegalArgumentException();	
		
		Node node = new Node(data);
		//index == 0 si vuole inserire il nodo in testa alla lista
		if(index == 0) {
			node.setNext(head);
			head = node;
		}
		Node tmp = head;
		//index > 1 quindi va inserito in mezzo
		for(int i = 1; i < index; ++i) {
			tmp = tmp.getNext();
		}
		
		if(tmp == null) 
			add(data);
		else
			tmp.setNext(node);
		
		counter++;
		
	}
 
	public Object get(int index) {
	// returns the element at the specified position in this list.
		if(index < 0 || index >= counter)
			throw new IllegalArgumentException();
		Node tmp = head;
		for(int i = 0; i < index; ++i)
			tmp = tmp.getNext();
		return tmp.getData();
 		
	}
 
	// removes the element at the specified position in this list.
	public boolean remove(int index) {
		if(index < 0 || index >= counter) 
			return false;
		
		if(index == 0) {
			head = head.getNext();
			counter--;
			return true;
		}
		
		//index >= 1 devo salvarmi il predecessore
		Node copy = head.getNext();
		Node pred = head;
		for(int i = 1; i < index; ++i) {
			if(copy != null) {
				pred = copy;
				copy = copy.getNext();
			}
		}
		
		if(copy != null) {
			pred.setNext(copy.getNext());
			copy.setNext(null);
			counter--;
			return true;
		}
  		return false;	
	}
 
	// returns the number of elements in this list.
	public int size() {
		return getCounter();
	}
 
	public String toString() {
		String help = " ";
		Node copy = head;
		for(int i = 1; i <= counter; ++i) {
			help += (String)copy.getData();
			copy = copy.getNext();
		}
		
		return help;
	}
	private int getCounter() {
		return counter;
	}
	private static class Node {//se non fosse statica ogni nodo avrebbe accesso ai campi dati prorpri della lista (logicamente non corretto)
		// reference to the next node in the chain, or null if there isn't one.
		Node next;
 
		// data carried by this node. could be of any type you need.
		Object data;
 
		// Node constructor
		public Node(Object dataValue) {
			next = null;
			data = dataValue;
		}
 
		// another Node constructor if we want to specify the node to point to.
		public Node(Object dataValue, Node nextValue) {
			data = dataValue;
			next = nextValue;
		}
 
		// these methods should be self-explanatory
		public Object getData() {
			return data;
		}
 
		@SuppressWarnings("unused")
		public void setData(Object dataValue) {
			data = dataValue;
		}
 
		public Node getNext() {
			return next;
		}
 
		public void setNext(Node nextValue) {
			next = nextValue;
		}
 
	}
	
	public static void main(String[] args) {
		 
		// Default constructor - let's put "0" into head element.
		LinkedList list = new LinkedList();
 
		// add more elements to LinkedList
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");
 
		/*
		 * Please note that primitive values can not be added into LinkedList directly. They must be converted to their
		 * corresponding wrapper class.
		 */
 
		System.out.println("Print: list: \t\t" + list);
		System.out.println(".size(): \t\t\t\t" + list.size());
		System.out.println(".get(3): \t\t\t\t" + list.get(3) + " (get element at index:3 - list starts from 0)");
		System.out.println(".remove(2): \t\t\t\t" + list.remove(2) + " (element removed)");
		System.out.println(".get(3): \t\t\t\t" + list.get(3) + " (get element at index:3 - list starts from 0)");
		System.out.println(".size(): \t\t\t\t" + list.size());
		System.out.println("Print again - list: \t" + list);
	}

}
