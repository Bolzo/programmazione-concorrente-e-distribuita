import java.io.IOException;
import lab1.statistics.*;


public class main{
    public static void main(String[] args) {
        MultiMap<Integer,Double> map = new MultiMap<>();
        try {
            PCD1819Statistics.buildMapFromInput(map, "lab1/statistics/esempi - IO file/input.txt");
        }
        catch(IOException errore){
            errore.printStackTrace();
        }
        
    }
}
